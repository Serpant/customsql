#ifndef CUSTOM_SQL_TABLE_H
#define CUSTOM_SQL_TABLE_H

#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include "Column.h"
#include "Record.h"
#include "Record_iterator.h"
#include "customSqlExceptions.h"
#include "sql_tools.h"

class Table;

struct TableInfo {
    explicit TableInfo(const Table& table);
    const std::string name;
    std::vector<ColumnInfo> columns;
    // primary key
};

class Table {
public:
    explicit Table(const std::string& name) : name_{createStrLower(name)} {
        if (not isValidName(name_)) {
            throw QueryParseErrorException{"'" + name_ +
                                           "' is not a valid table name"};
        }
    };
    ~Table() = default;
    Table& operator=(Table&& other) noexcept {
        name_ = std::move(other.name_);
        columns_ = std::move(other.columns_);

        return *this;
    }
    Table(Table&& other) noexcept
        : name_{std::move(other.name_)}, columns_{std::move(other.columns_)} {}
    Table(const Table& other) = default;
    Table& operator=(const Table& other) = default;

    void set_name(const std::string& new_name) {
        if (not isValidName(new_name)) {
            throw InvalidNameException{new_name};
        }

        name_ = new_name;
    }

    void add_column(const std::string& columnName, const Column::Type& type,
                    bool nullable = true);

    [[nodiscard]] const auto& name() const { return name_; }
    [[nodiscard]] auto columnNumber() const { return columns_.size(); }
    [[nodiscard]] auto recordsNumber() const -> size_t;

    [[nodiscard]] auto begin() const {
        Record_iterator row;
        for (const auto& column : columns_) {
            row.add_field(column.name(), column.begin());
        }
        return row;
    }
    [[nodiscard]] auto end() const {
        Record_iterator row;
        for (const auto& column : columns_) {
            row.add_field(column.name(), column.end());
        }
        return row;
    }

    [[nodiscard]] auto columnBegin() { return columns_.begin(); }
    [[nodiscard]] auto columnEnd() { return columns_.end(); }
    [[nodiscard]] auto columnBegin() const { return columns_.cbegin(); }
    [[nodiscard]] auto columnEnd() const { return columns_.cend(); }

    void eraseColumn(std::vector<Column>::iterator pos) { columns_.erase(pos); }

    [[nodiscard]] auto getColumnNamed(const std::string& column_name) {
        return std::find_if(columns_.begin(), columns_.end(),
                            [column_name](const Column& column) {
                                return column.name() ==
                                       createStrLower(column_name);
                            });
    }
    [[nodiscard]] auto getColumnNamed(const std::string& column_name) const {
        return std::find_if(columns_.cbegin(), columns_.cend(),
                            [column_name](const Column& column) {
                                return column.name() ==
                                       createStrLower(column_name);
                            });
    }

    [[nodiscard]] auto getColumnNamedChecked(const std::string& column_name) {
        if (auto pos = getColumnNamed(column_name); pos != columnEnd()) {
            return pos;
        }
        throw UnknownNameException{"column", column_name};
    }
    [[nodiscard]] auto getColumnNamedChecked(
        const std::string& column_name) const {
        if (auto pos = getColumnNamed(column_name); pos != columnEnd()) {
            return pos;
        }
        throw UnknownNameException{"column", column_name};
    }

    // (kenny) TODO: rename table function
    // (kenny) TODO: test for adding / removing when there are no columns
    void add_record(const Record& record);
    void add_record(Record&& record);

    // returns Record to fill in values
    [[nodiscard]] auto get_form() const {
        // (kenny) TODO: form should have contain type that is used
        return Record{columnBegin(), columnEnd()};
    }

    void remove_record(size_t index);
    void remove_record(Record_iterator pos);

    [[nodiscard]] auto find(const Field& field) const
        -> std::vector<Record_iterator>;

    [[nodiscard]] TableInfo description() const { return TableInfo{*this}; }

private:
    std::string name_;
    std::vector<Column> columns_;
    // (kenny): TODO: primary key
};

#endif  // !CUSTOM_SQL_TABLE_H

#include "Sql_interpreter.h"

#include <algorithm>
#include <filesystem>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <numeric>
#include <sstream>
#include <string>

#include "Query.h"
#include "customSqlExceptions.h"

[[nodiscard]] auto Sql_interpreter::getTableNamed(
    const std::string& table_name) {
    return std::find_if(
        tables_.begin(), tables_.end(),
        [table_name](const auto& table) { return table.name() == table_name; });
}

[[nodiscard]] auto Sql_interpreter::getTableNamedChecked(
    const std::string& table_name) {
    auto pos = getTableNamed(table_name);
    if (pos == tables_.end()) {
        throw UnknownNameException{"table", table_name};
    }

    return pos;
};

void Sql_interpreter::run() {
    std::string input;
    while (true) {
        std::cout << sql::prompt;
        std::getline(std::cin, input, sql::end_input_sign);

        trim(input, " \t\n");
        if (std::cin.eof() || input == sql::keyword::exit) {
            break;
        }

        parseQuery(input);
    }
}

bool Sql_interpreter::parseQuery(const std::string& input) try {
    // (kenny) TODO: try to check for syntax error first, then for logical
    // errors e.g. cannot find column or bad value type
    // WARNING: "table.column" syntax not handled yet
    // (kenny): TODO: readCommaSeparatedValuesInsideParentheses
    // (kenny): TODO: condition function
    // (kennY) TODO: handle multiple databases

    query.set_value(input);
    auto actionName = query.getToken();
    auto pos = actions.find(actionName);
    if (pos == actions.end()) {
        throw QueryParseErrorException{"Unknown action '" + actionName + "'"};
    }

    auto action = pos->second;
    // call appropriate function
    (this->*(action))();

    return true;
} catch (const CustomSqlException& e) {
    std::cerr << "[ERROR]: " << e.what() << '\n';
    return false;
} catch (const std::exception& e) {
    std::cerr << e.what() << '\n';
    return false;
}

void Sql_interpreter::describeTable(const std::string& tableName) {
    if (tableName.empty()) {
        throw QueryParseErrorException{"Table name cannot be empty"};
    }

    auto table = getTableNamedChecked(tableName);
    TableInfo tableInfo = table->description();
    std::cout << "Table: " << tableInfo.name << '\n';
    size_t columnNumber = 1;
    for (const auto& column : tableInfo.columns) {
        std::cout << columnNumber << ": " << column.name << " ["
                  << ColumnTypeAsString(column.type) << "] ";
        if (not column.nullable) {
            std::cout << "NOT NULL";
        }
        std::cout << '\n';
        ++columnNumber;
    }
}

void Sql_interpreter::alterTable(const std::string& tableName) {
    auto table = getTableNamedChecked(tableName);
    if (auto action = query.getToken(); action == sql::keyword::add) {
        auto columnName = query.getToken();
        auto type_name = query.getToken();
        auto type = stringAsColumnType(type_name);
        if (not type.has_value()) {
            throw QueryParseErrorException{"Incorrect type name '" + type_name +
                                           '\''};
        }

        table->add_column(columnName, type.value());
    } else if (action == sql::keyword::drop) {
        query.expect(sql::object::column);

        auto columnName = query.getToken();
        auto column = table->getColumnNamedChecked(columnName);

        table->eraseColumn(column);
    } else if (action == sql::keyword::modify) {
        // (kenny): TODO: changing column type
        throw QueryParseErrorException{"'MODIFY' Is not implemented yet"};
    } else if (action == sql::keyword::rename) {
        if (auto object = query.getToken(); object == sql::object::column) {
            auto old_name = query.getToken();
            query.expect(sql::keyword::to);
            auto new_name = query.getToken();

            auto column = table->getColumnNamedChecked(old_name);
            column->set_name(new_name);
        } else if (object == sql::keyword::to) {  // rename table
            auto new_name = query.getToken();
            table->set_name(new_name);
        } else {
            throw QueryParseErrorException{"Unknown object '" + object + '\''};
        }
    } else {
        throw QueryParseErrorException{"Unknown action '" + action +
                                       "'when altering table"};
    }
}

// (kenny): TODO: refactor
void Sql_interpreter::selectAction() {
    // read columns;
    auto columns = getCommaSeparatedValues();
    query.expect(sql::keyword::from);
    // get table name
    auto tableName = query.getToken();
    auto table = getTableNamedChecked(tableName);
    std::vector<std::vector<Column>::iterator> cols;
    for (const auto& colName : columns) {
        if (colName == "*") {
            // get all columns
            for (auto column = table->columnBegin();
                 column != table->columnEnd(); ++column) {
                cols.push_back(column);
            }
            continue;
        }

        auto pos = table->getColumnNamedChecked(colName);
        cols.push_back(pos);
    }

    // (kenny): TODO: create condition function that analyze query and returns
    // true/false condition
    std::vector<size_t> positions(table->recordsNumber());
    std::iota(positions.begin(), positions.end(), 0);
    // (kenny) TODO: create function if (query.nextToken(someToken)) then ...
    if (query.previewToken() == sql::keyword::where) {
        query.expect(sql::keyword::where);

        // condition start
        std::string columnName = query.getToken();
        query.expect("=");
        Column::Value value = query.getToken();
        if (value == sql::keyword::null) {
            value = std::nullopt;
        }

        auto localTable = getTableNamedChecked(tableName);
        auto column = localTable->getColumnNamedChecked(columnName);
        // leave only positions that match requirements

        auto invalid = std::find_if_not(positions.begin(), positions.end(),
                                        [&column, &value](auto index) {
                                            return (*column)[index] == value;
                                        });
        while (invalid != positions.end()) {
            positions.erase(invalid);
            invalid = std::find_if_not(invalid, positions.end(),
                                       [&column, &value](auto index) {
                                           return (*column)[index] == value;
                                       });
        }
    }

    if (query.previewToken() == sql::keyword::order) {
        query.expect(sql::keyword::order);
        query.expect(sql::keyword::by);
        auto column = table->getColumnNamedChecked(query.getToken());

        bool descending = false;
        if (query.previewToken() == sql::keyword::desc) {
            query.ignore();
            descending = true;
        }

        if (descending) {
            std::sort(positions.begin(), positions.end(),
                      [&column](auto i_left, auto i_right) {
                          return (*column)[i_left] > (*column)[i_right];
                      });
        } else {
            std::sort(positions.begin(), positions.end(),
                      [&column](auto i_left, auto i_right) {
                          return (*column)[i_left] < (*column)[i_right];
                      });
        }
    }

    constexpr int space = 10;
    // print headers
    for (const auto& column : cols) {
        std::cout << std::setw(space) << column->name() << ' ';
    }
    std::cout << '\n';
    // print records;
    for (auto index : positions) {
        for (const auto& column : cols) {
            std::cout << std::setw(space) << (*column)[index].value_or("NULL")
                      << ' ';
        }
        std::cout << '\n';
    }
}

void Sql_interpreter::deleteAction() {
    query.expect(sql::keyword::from);
    std::string tableName = query.getToken();

    query.expect(sql::keyword::where);
    // condition start
    std::string columnName = query.getToken();
    query.expect("=");
    Column::Value value = query.getToken();
    if (value == sql::keyword::null) {
        value = std::nullopt;
    }

    auto table = getTableNamedChecked(tableName);
    auto column = table->getColumnNamedChecked(columnName);
    auto pos = std::find(column->begin(), column->end(), value);
    while (pos != column->end()) {
        auto index = pos - column->begin();
        table->remove_record(index);
        pos = std::find(pos, column->end(), value);
    }
}

void Sql_interpreter::createAction() {
    if (auto object = query.getToken(); object == sql::object::database) {
        /*
        auto database_name = query.getToken();
        if (not isValidName(database_name)) {
            throw QueryParseErrorException{"'" + database_name +
                                           "' is not a valid database name"};
        }

        */
        std::cout << "DATABASE is not implemented yet\n";
    } else if (object == sql::object::table) {
        auto table_name = query.getToken();
        Table table{table_name};

        if (query.areTokensLeft()) {
            query.expect("(");

            while (true) {
                auto columnName = query.getToken();
                if (columnName == ")") {
                    break;
                }
                if (not isValidName(columnName)) {
                    throw QueryParseErrorException{"Invalid column name '" +
                                                   columnName + "'"};
                }

                auto type_name = query.getToken();
                auto type = stringAsColumnType(type_name);
                if (not type.has_value()) {
                    throw QueryParseErrorException{"Unknown type '" +
                                                   type_name + '\''};
                }

                bool nullable = true;
                if (query.previewToken() == sql::keyword::sql_not) {
                    query.ignore();
                    query.expect(sql::keyword::null);
                    nullable = false;
                }
                table.add_column(columnName, type.value(), nullable);

                if (auto sep = query.getToken(); sep == ")") {
                    break;  // stop reading
                } else if (sep != ",") {
                    throw QueryParseErrorException{"Unknown token '" + sep +
                                                   "' expected ',' or ')'"};
                }
                // semicolon continue reading
            }
        }

        if (getTableNamed(table.name()) != tables_.end()) {
            throw QueryParseErrorException{"Table with name '" + table.name() +
                                           "' already exist"};
        }

        tables_.emplace_back(std::move(table));
    } else {
        std::cout << "Unknown object type after 'CREATE'\n";
    }
}

void Sql_interpreter::dropAction() {
    if (auto object = query.getToken(); object == sql::object::database) {
        auto database_name = query.getToken();
        if (not isValidName(database_name)) {
            throw QueryParseErrorException{"'" + database_name +
                                           "' is not a valid database name"};
        }

        std::cout << "DATABASE is not implemented yet\n";
    } else if (object == sql::object::table) {
        auto table_name = query.getToken();
        if (not isValidName(table_name)) {
            throw QueryParseErrorException{"'" + table_name +
                                           "' is not a valid table name"};
        }

        auto table = getTableNamedChecked(table_name);
        tables_.erase(table);
    } else {
        std::cout << "Unknown object type after 'CREATE'\n";
    }
}

void Sql_interpreter::showAction() {
    // (kenny): TODO: SHOW DATABASES;
    if (auto object = query.getToken(); object == sql::object::tables) {
        if (tables_.empty()) {
            std::cout << "No Tables in database\n";
            return;
        }
        for (const auto& table : tables_) {
            std::cout << table.name() << '\n';
        }
    } else {
        throw QueryParseErrorException{"Incorrect object '" + object +
                                       "' after 'SHOW'"};
    }
}
void Sql_interpreter::alterAction() {
    if (auto object = query.getToken(); object == sql::object::table) {
        auto tableName = query.getToken();
        alterTable(tableName);
    } else {
        throw QueryParseErrorException{"Unknown object '" + object +
                                       "'type after alter keyword"};
    }
}

void Sql_interpreter::insertAction() {
    query.expect(sql::keyword::into);
    auto tableName = query.getToken();
    auto table = getTableNamedChecked(tableName);

    std::vector<std::string> columns;
    if (query.previewToken() == "(") {
        columns = getValuesInsideParentheses();
    } else {  // get normal order of columns
        for (auto column = table->columnBegin(); column != table->columnEnd();
             ++column) {
            columns.push_back(column->name());
        }
    }

    query.expect(sql::keyword::values);
    query.expect("(");

    auto record = table->get_form();

    for (const auto& field_name : columns) {
        auto token = query.getToken();
        if (token != sql::keyword::null) {
            record.get_field(field_name) = token;
        } else {
            record.get_field(field_name) = std::nullopt;
        }

        if (auto separator = query.getToken(); separator == ")") {
            break;
        } else if (separator != ",") {
            throw QueryParseErrorException{"Missing ')' or ',' After '" +
                                           token + "' value"};
        }
    }
    table->add_record(record);
}

void Sql_interpreter::updateAction() {
    auto tableName = query.getToken();
    auto table = getTableNamedChecked(tableName);
    query.expect(sql::keyword::set);
    auto columnName = query.getToken();
    auto column = table->getColumnNamedChecked(columnName);
    query.expect("=");

    Column::Value value = query.getToken();
    if (value == sql::keyword::null) {
        value = std::nullopt;
    }

    query.expect(sql::keyword::where);
    // <condition> // (kenny): TODO: Try to create function condition();
    auto conditionColumnName = query.getToken();
    auto conditionColumn = table->getColumnNamedChecked(conditionColumnName);
    query.expect("=");
    Column::Value conditionValue = query.getToken();
    if (conditionValue == sql::keyword::null) {
        conditionValue = std::nullopt;
    }
    // actual update;
    std::vector<long> records_to_update;
    for (auto pos = conditionColumn->begin(); pos != conditionColumn->end();
         ++pos) {
        if (*pos == conditionValue) {
            records_to_update.push_back(pos - conditionColumn->begin());
        }
    }

    for (const auto index : records_to_update) {
        auto pos = column->begin() + index;
        *pos = value;
    }
}

void Sql_interpreter::saveToFile(const std::filesystem::path& filename) {
    std::ofstream dumpDB{filename};
    namespace fs = std::filesystem;
    if (!dumpDB) {
        throw std::runtime_error{"Cannot open file named '" +
                                 fs::absolute(filename).string() +
                                 "' for writing"};
    }

    dumpDB << "-- database structure\n";
    for (auto table = tables_begin(); table != tables_end(); ++table) {
        auto tableInfo = table->description();
        dumpDB << "CREATE TABLE " << table->name() << "(\n\t";
        auto column = table->columnBegin();
        while (column != table->columnEnd()) {
            dumpDB << column->name() << ' '
                   << ColumnTypeAsString(column->type()) << ' ';
            if (not column->nullable()) {
                dumpDB << "NOT NULL";
            }
            dumpDB << ",\n\t";
            ++column;
        }
        dumpDB << ");\n";
    }

    dumpDB << "-- database data\n";
    for (auto table = tables_begin(); table != tables_end(); ++table) {
        for (const auto& record : *table) {
            dumpDB << "INSERT INTO " << table->name() << " VALUES (";
            // get fields in correct order
            auto column = table->columnBegin();
            while (column != table->columnEnd()) {
                const auto& value = *record.at(column->name());
                dumpDB << value.value_or("NULL");

                ++column;
                if (column != table->columnEnd()) {
                    dumpDB << ", ";
                }
            }
            dumpDB << ')' << sql::end_input_sign << '\n';
        }
        dumpDB << "\n";
    }
}
void Sql_interpreter::dumpAction() {
    namespace fs = std::filesystem;
    fs::path filename = query.getToken();
    if (query.areTokensLeft()) {
        throw QueryParseErrorException{
            "Unexpected token '" + query.getToken() + "' after filename '" +
            filename.string() +
            "' in 'DUMP'\n[HINT]: Enclose filename in quotes"};
    }

    if (not filename.has_extension()) {
        std::cout << "No extension given using default '" << sql::extension
                  << "'\n";
        filename.replace_extension(sql::extension);
    }
    saveToFile(filename);

    std::cout << "Successfully saved in " << fs::absolute(filename) << '\n';
}

int Sql_interpreter::readFromFile(const std::filesystem::path& filename) {
    namespace fs = std::filesystem;
    if (not fs::exists(filename)) {
        throw std::runtime_error{"File '" + fs::absolute(filename).string() +
                                 "' does not exist"};
    }

    std::ifstream sourceDB{filename};
    if (not sourceDB) {
        throw std::runtime_error{"Cannot open '" +
                                 fs::absolute(filename).string() +
                                 "' for reading"};
    }

    int errors = 0;
    for (std::string line; std::getline(sourceDB, line, sql::end_input_sign);) {
        if (not parseQuery(line)) {
            ++errors;
        }
        sourceDB >> std::ws;
    }

    return errors;
}

void Sql_interpreter::sourceAction() {
    namespace fs = std::filesystem;

    if (not tables_.empty()) {
        throw QueryParseErrorException{
            "Database should be empty to allow reading from file"};
    }

    auto filename = query.getToken();

    if (query.areTokensLeft()) {
        throw QueryParseErrorException{
            "Unexpected token '" + query.getToken() + "' after filename '" +
            filename + "' in 'SOURCE'\n[HINT]: Enclose filename in quotes"};
    }

    auto errorsNumber = readFromFile(filename);
    if (errorsNumber == 0) {
        std::cout << "Successfully read from " << fs::absolute(filename)
                  << '\n';
    } else {
        std::cout << errorsNumber << " errors occurred when reading from "
                  << fs::absolute(filename) << '\n';
    }
}

void Sql_interpreter::describeAction() {
    auto tableName = query.getToken();

    describeTable(tableName);
}
auto Sql_interpreter::getCommaSeparatedValues() -> std::vector<std::string> {
    std::vector<std::string> values;
    while (true) {
        auto value = query.getToken();
        values.push_back(std::move(value));

        if (query.previewToken() != ",") {
            break;
        }

        query.ignore();  // throw away that comma and read next table
    }

    return values;
}

auto Sql_interpreter::getValuesInsideParentheses() -> std::vector<std::string> {
    std::vector<std::string> values;

    query.expect("(");
    while (true) {
        auto value = query.getToken();
        if (value == ")") {
            break;
        }
        values.push_back(std::move(value));

        if (auto sep = query.getToken(); sep == ")") {
            break;  // stop reading
        } else if (sep != ",") {
            throw QueryParseErrorException{"Unknown token '" + sep +
                                           "' expected ',' or ')'"};
        }
        // semicolon continue reading
    }

    return values;
}

auto check_database(const std::vector<Table>& tables)
    -> std::optional<std::string> {
    // (kenny): TODO: check database
    for (const auto& table : tables) {
        if (not isValidName(table.name())) {
            return "Invalid table name '" + table.name() + '\'';
        }

        // check for invalid column names
        for (auto column = table.columnBegin(); column != table.columnEnd();
             ++column) {
            if (not isValidName(column->name())) {
                return "Invalid column name '" + column->name() + '\'';
            }

            for (const auto& value : *column) {
                // check for null values in not nullable columns
                if (not column->nullable() && not value.has_value()) {
                    return "Null value in NOT NULL column '" + column->name() +
                           '\'';
                }
                // check if correct value type if not null
                if (value.has_value() and
                    not isCorrectType(column->type(), *value)) {
                    return "Incorrect type in '" + column->name() +
                           "' expected type " +
                           ColumnTypeAsString(column->type()) + " value = '" +
                           *value + '\'';
                }
            }
        }
    }

    return std::nullopt;
}

void Sql_interpreter::checkAction() {
    if (query.areTokensLeft()) {
        throw QueryParseErrorException{"Unknown tokens after 'CHECK'"};
    }

    if (auto error_msg = check_database(tables_); error_msg.has_value()) {
        std::cout << "Integrity error: " << error_msg.value() << '\n';
    } else {
        std::cout << "Done, no erros occurred\n";
    }
}

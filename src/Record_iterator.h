#ifndef CUSTOM_SQL_RECORD_ITERATOR_H
#define CUSTOM_SQL_RECORD_ITERATOR_H

#include <map>
#include <optional>
#include <string>
#include <vector>

#include "Column.h"

class Record_iterator {
public:
    using Column_iterator = std::vector<Column::Value>::const_iterator;
    using field_name = std::string;

public:
    void add_field(const std::string& fieldName, Column_iterator fieldPos) {
        positions_.emplace(fieldName, fieldPos);
    }
    //[[nodiscard]] const auto& get_fields() const { return positions_; }
    [[nodiscard]] auto fieldNumber() const { return positions_.size(); }

    // WARNING: throws out_of_range if not found
    [[nodiscard]] auto& getField(const std::string& fieldName) {
        return positions_.at(fieldName);
    };
    // WARNING: throws out_of_range if not found
    [[nodiscard]] const auto& getField(const std::string& fieldName) const {
        return positions_.at(fieldName);
    };
    [[nodiscard]] auto& getValueOf(const std::string& fieldName) {
        return *positions_.at(fieldName);
    };
    // WARNING: throws out_of_range if not found
    [[nodiscard]] const auto& getValueOf(const std::string& fieldName) const {
        return *positions_.at(fieldName);
    };

    auto operator*() { return positions_; }

    void operator++() {
        for (auto& [field, field_pos] : positions_) {
            ++field_pos;
        }
    }
    void operator--() {
        for (auto& [field, field_pos] : positions_) {
            --field_pos;
        }
    }

    void next(int64_t length) {
        for (auto& [field, field_pos] : positions_) {
            field_pos += length;
        }
    }
    void prev(int64_t length) {
        for (auto& [field, field_pos] : positions_) {
            field_pos -= length;
        }
    }

    bool operator==(const Record_iterator& other) {
        return positions_ == other.positions_;
    }
    bool operator!=(const Record_iterator& other) {
        return positions_ != other.positions_;
    }

private:
    std::map<field_name, Column_iterator> positions_;
};

#endif  // !CUSTOM_SQL_RECORD_ITERATOR_H

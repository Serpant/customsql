#ifndef CUSTOM_SQL_COLUMN_H
#define CUSTOM_SQL_COLUMN_H

#include <algorithm>
#include <optional>
#include <string>
#include <variant>
#include <vector>

#include "customSqlExceptions.h"
#include "sql_tools.h"

struct ColumnInfo;

// (kenny): TODO: make size limit for string
class Column {
public:
    using Value = std::optional<std::string>;

    enum class Type {
        Integer,
        // floating,
        // boolean,
        Text,
    };

    Column() = delete;

    Column(std::string name, Column::Type type,
           std::vector<Value>::size_type rows = 0, bool nullable = true)
        : type_{type},
          name_{std::move(name)},
          values_(rows, std::nullopt),
          nullable_{nullable} {
        if (not isValidName(name_)) {
            throw InvalidNameException{name_};
        }
    }

    [[nodiscard]] bool nullable() const { return nullable_; }

    void set_name(const std::string& new_name) {
        if (not isValidName(new_name)) {
            throw InvalidNameException{new_name};
        }

        name_ = new_name;
    }

    [[nodiscard]] auto begin() const noexcept { return values_.begin(); }
    [[nodiscard]] auto end() const noexcept { return values_.end(); }
    [[nodiscard]] auto begin() noexcept { return values_.begin(); }
    [[nodiscard]] auto end() noexcept { return values_.end(); }

    [[nodiscard]] ColumnInfo describe();
    [[nodiscard]] auto size() const noexcept { return values_.size(); }
    [[nodiscard]] auto name() const noexcept { return name_; }
    [[nodiscard]] auto type() const noexcept { return type_; }

    // (kenny) TODO: try Rvalue
    void push_back(Value value);

    [[nodiscard]] auto operator[](
        std::vector<std::string>::size_type index) const -> Column::Value;

    void erase(std::vector<Value>::const_iterator pos) { values_.erase(pos); }

private:
    Column::Type type_;
    std::string name_;
    std::vector<Value> values_;
    bool nullable_;

    // (kenny): TODO: auto_increment
    // (kenny): TODO: bool unique
};

struct ColumnInfo {
    explicit ColumnInfo(const Column& column)
        : name{column.name()},
          type{column.type()},
          nullable{column.nullable()} {}
    const std::string name;
    const Column::Type type;
    bool nullable;
    // bool isUniqueRequired;
};

constexpr const char* ColumnTypeAsString(Column::Type type) {
    switch (type) {
        case Column::Type::Integer:
            return "Integer";
        case Column::Type::Text:
            return "Text";
        default:
            return "Undefined";
    }
}

// WARNING: function handle only lower case
constexpr std::optional<Column::Type> stringAsColumnType(
    std::string_view value) {
    if (value == "integer") {
        return Column::Type::Integer;
    }
    if (value == "text") {
        return Column::Type::Text;
    }

    return std::nullopt;
}

constexpr bool isCorrectInteger(std::string_view value) {
    // cannot make value string_view because regex does not support
    // string_view return regex_match(value, regex{R"(-?\d+)"});
    if (value.empty()) {
        return false;
    }

    if (value.front() == '-') {
        if (value.size() == 1) {
            return false;
        }

        // skip first character
        return std::all_of(value.begin() + 1, value.end(), ::isdigit);
    }
    return std::all_of(value.begin(), value.end(), ::isdigit);
}

// (kenny): TODO: handle bool and floating point numbers
constexpr bool isCorrectType(Column::Type type, std::string_view value) {
    switch (type) {
        /* TODO:  move types that are handled
            case ColumnType::floating:
                if (isCorrectFloating(value))
                    return true;
                break;
            case ColumnType::boolean:
                if (isCorrect(value))
                    return true;
                break;
            */
        case Column::Type::Integer:
            if (!value.empty() && isCorrectInteger(value)) {
                return true;
            }
            break;
        case Column::Type::Text:
            return true;  // string is always a proper text
    }

    return false;
}

#endif  // CUSTOM_SQL_COLUMN_H

#include "Column.h"

#include <optional>
#include <regex>
#include <string>
#include <vector>

#include "customSqlExceptions.h"

using std::regex;
using std::string;
using std::vector;

void Column::push_back(Value value) {
    if (not nullable_ and not value.has_value()) {
        throw NullNotAllowedException{this->name()};
    }
    if (value.has_value() and not isCorrectType(type_, *value)) {
        throw WrongArgumentTypeException{name(), ColumnTypeAsString(type()),
                                         *value};
    }

    values_.push_back(std::move(value));
}

[[nodiscard]] auto Column::operator[](vector<string>::size_type index) const
    -> Column::Value {
    return values_[index];
}

[[nodiscard]] ColumnInfo Column::describe() { return ColumnInfo{*this}; }

#ifndef CUSTOM_SQL_TOOLS_H
#define CUSTOM_SQL_TOOLS_H

#include <algorithm>
#include <iostream>
#include <set>

inline bool isSqlKeyword(std::string_view name) {
    // (kenny) TODO: try to use names from sql::keyword namespace
    static std::set<std::string_view> keywords = {
        "exit",   "select", "from",   "desc",   "describe", "insert",
        "set",    "into",   "values", "alter",  "delete",   "create",
        "use",    "drop",   "modify", "show",   "update",   "add",
        "where",  "source", "dump",   "table",  "tables",   "database",
        "column", "null",   "check",  "rename", "to"};

    return keywords.find(name) != keywords.end();
}

inline constexpr bool isValidName(std::string_view name) noexcept {
    return not name.empty() and not isSqlKeyword(name) and
           std::all_of(name.begin(), name.end(), [](char ch) {
               return std::isalnum(ch) != 0 or ch == '_';
           });
}

inline std::string createStrLower(std::string_view str) {
    std::string lower{str};
    std::transform(lower.begin(), lower.end(), lower.begin(), ::tolower);
    return lower;
}

#endif  // !CUSTOM_SQL_TOOLS_H

#include <iostream>

#include "Sql_interpreter.h"

auto main() -> int try {
    Sql_interpreter customSql;

    customSql.run();

    return 0;
} catch (const std::exception& e) {
    std::cerr << e.what() << '\n';
}

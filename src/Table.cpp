#include "Table.h"

#include <algorithm>
#include <cassert>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include "Column.h"
#include "Record.h"
#include "customSqlExceptions.h"
#include "sql_tools.h"

using std::string;
using std::vector;

void Table::add_column(const std::string& columnName, const Column::Type& type,
                       bool nullable) {
    if (getColumnNamed(columnName) != columnEnd()) {
        throw ColumnAlreadyExistException{name(), columnName};
    }

    if (not isValidName(columnName)) {
        throw InvalidNameException{columnName};
    }

    columns_.emplace_back(createStrLower(columnName), type, recordsNumber(),
                          nullable);
};

[[nodiscard]] auto Table::recordsNumber() const -> size_t {
    if (!columns_.empty()) {
        return columns_.front().size();  // get size from first column
    }
    return 0;
}

// check if values are right to fit in table throws otherwise
void throwIfInputDataNotRight(const Record& record, const Table& table) {
    if (table.columnNumber() != record.fieldNumber()) {
        throw WrongArgumentNumberException{table.columnNumber(),
                                           record.fieldNumber()};
    }

    for (const auto& [fieldName, fieldValue] : record) {
        auto column = table.getColumnNamedChecked(fieldName);
        // (kenny): TODO: check for nullable value and if the column can be null
        //  we have column

        if (not column->nullable() and not fieldValue.has_value()) {
            throw NullNotAllowedException{column->name()};
        }
        if (fieldValue.has_value() and
            not isCorrectType(column->type(), fieldValue.value())) {
            throw WrongArgumentTypeException{column->name(),
                                             ColumnTypeAsString(column->type()),
                                             fieldValue.value()};
        }
    }
}

// MAY THROW
void Table::add_record(const Record& record) {
    throwIfInputDataNotRight(record, *this);

    // dumb insert assert that previous check is correct
    for (const auto& [fieldName, fieldValue] : record) {
        auto column = getColumnNamed(fieldName);
        // WARNING: no check against column == end()

        column->push_back(fieldValue);
    }
}
// MAY THROW
void Table::add_record(Record&& record) {
    throwIfInputDataNotRight(record, *this);

    for (auto& [fieldName, fieldValue] : record) {
        auto column = getColumnNamed(fieldName);
        // WARNING: no check against column == end()

        column->push_back(fieldValue);
    }
}

void Table::remove_record(Record_iterator pos) {
    if (pos.fieldNumber() != columns_.size()) {
        std::ostringstream msg;
        msg << "Remove_record(Record_iterator): Field number ("
            << pos.fieldNumber() << ") differ from column number ("
            << columns_.size() << ")";
        throw std::runtime_error{msg.str()};
    }

    for (auto& column : columns_) {
        column.erase(pos.getField(column.name()));
    }
}

// (kenny): TODO: switch row at position <index> with <last_element> to prevent
// copying and copying to left all elements at right to the deleted position
void Table::remove_record(size_t index) {
    // Do nothing when index is out of range
    // index is size_t so cannot be null
    if (index >= recordsNumber()) {
        return;
    }

    // HACK: casting to long because operator+() does not take size_t
    for (auto& column : columns_) {
        auto field = column.begin() + static_cast<std::int64_t>(index);
        column.erase(field);
    }
}

[[nodiscard]] auto Table::find(const Field& field) const
    -> std::vector<Record_iterator> {
    auto matching_column = getColumnNamed(field.name());
    if (matching_column == columns_.end()) {
        throw UnknownNameException{"column", field.name()};
    }

    std::vector<Record_iterator> positions;
    for (auto record = this->begin(); record != this->end(); ++record) {
        if (record.getValueOf(field.name()) == field.value()) {
            positions.push_back(record);
        }
    }

    return positions;
}

TableInfo::TableInfo(const Table& table) : name{table.name()} {
    auto currentColumnt = table.columnBegin();
    while (currentColumnt != table.columnEnd()) {
        columns.emplace_back(*currentColumnt);
        ++currentColumnt;
    }
}

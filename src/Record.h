#ifndef CUSTOM_SQL_RECORD_H
#define CUSTOM_SQL_RECORD_H

#include <map>
#include <string>
#include <vector>

#include "Column.h"

struct Field {
public:
    Field(std::string columnName, Column::Value value)
        : columnName_{std::move(columnName)}, value_{std::move(value)} {};

    [[nodiscard]] const auto& name() const { return columnName_; }
    [[nodiscard]] const auto& value() const { return value_; }

private:
    std::string columnName_;
    Column::Value value_;
};

class Record {
public:
    Record(std::vector<Column>::const_iterator first,
           std::vector<Column>::const_iterator last) {
        while (first != last) {
            fields_.emplace(first->name(), std::nullopt);
            ++first;
        }
    }

    /*
    Record(Table ,
           index) {
        // return record with given index in table
    }
    */
    [[nodiscard]] auto fieldNumber() const { return fields_.size(); }

    [[nodiscard]] auto begin() const { return fields_.cbegin(); }
    [[nodiscard]] auto end() const { return fields_.cend(); }
    [[nodiscard]] auto begin() { return fields_.begin(); }
    [[nodiscard]] auto end() { return fields_.end(); }

    [[nodiscard]] auto& get_field(const std::string& field_name) {
        return fields_.at(field_name);
    }

private:
    // (kenny) TODO: try to hold type too
    std::map<std::string, Column::Value> fields_;  // we need unique keys
};

#endif  // !CUSTOM_SQL_RECORD_H

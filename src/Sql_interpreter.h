#ifndef CUSTOM_SQL_SQL_INTERPRETER_H
#define CUSTOM_SQL_SQL_INTERPRETER_H

#include <filesystem>
#include <string>
#include <string_view>
#include <unordered_map>

#include "Query.h"
#include "Table.h"

// exists to indicate correct checking of correct keywords
// keywords are lowercase
// e.g. somebody could fatally check for token == "SELECT"
namespace sql {
namespace keyword {

constexpr std::string_view exit = "exit";

constexpr std::string_view select = "select";
constexpr std::string_view from = "from";
constexpr std::string_view desc = "desc";
constexpr std::string_view describe = "describe";
constexpr std::string_view insert = "insert";
constexpr std::string_view set = "set";
constexpr std::string_view into = "into";
constexpr std::string_view sql_not = "not";
constexpr std::string_view null = "null";
constexpr std::string_view values = "values";
constexpr std::string_view alter = "alter";
// INFO: 'delete' is c++ keyword
constexpr std::string_view sql_delete = "delete";
constexpr std::string_view create = "create";
constexpr std::string_view use = "use";
constexpr std::string_view drop = "drop";
constexpr std::string_view modify = "modify";
constexpr std::string_view rename = "rename";
constexpr std::string_view to = "to";
constexpr std::string_view check = "check";
constexpr std::string_view show = "show";
constexpr std::string_view update = "update";
constexpr std::string_view add = "add";
constexpr std::string_view where = "where";
constexpr std::string_view order = "order";
constexpr std::string_view by = "by";

constexpr std::string_view source = "source";
constexpr std::string_view dump = "dump";
}  // namespace keyword

namespace object {
constexpr std::string_view table = "table";
constexpr std::string_view tables = "tables";
constexpr std::string_view database = "database";
constexpr std::string_view column = "column";

}  // namespace object

constexpr char end_input_sign = ';';
constexpr std::string_view prompt = "(sql): ";
constexpr std::string_view extension = "custsql";
}  // namespace sql

class Sql_interpreter {
    using action_t = void (Sql_interpreter::*const)();
    using Actions_t = std::unordered_map<std::string_view, action_t>;

public:
    void run();
    // returns true on success and false on failure
    bool parseQuery(const std::string& input);

    // (kenny): TODO: Create Database class
    // methods below should belong to Database class:
    [[nodiscard]] auto tables_begin() noexcept { return tables_.begin(); }
    [[nodiscard]] auto tables_end() noexcept { return tables_.end(); }
    [[nodiscard]] auto tables_cbegin() const noexcept {
        return tables_.cbegin();
    }
    [[nodiscard]] auto tables_cend() const noexcept { return tables_.cend(); }

    [[nodiscard]] auto getTableNamed(const std::string& table_name);
    // (kenny) TODO: make function name shorter
    [[nodiscard]] auto getTableNamedChecked(const std::string& table_name);

private:
    void selectAction();
    void describeAction();
    void deleteAction();
    void createAction();
    void dropAction();
    void showAction();
    void insertAction();
    void alterAction();
    void updateAction();
    void dumpAction();
    void sourceAction();
    // checks database integrity
    void checkAction();

    auto getCommaSeparatedValues() -> std::vector<std::string>;
    auto getValuesInsideParentheses() -> std::vector<std::string>;
    void alterTable(const std::string& tableName);
    void describeTable(const std::string& tableName);

    void saveToFile(const std::filesystem::path& filename);
    // returns number of erros occurred
    int readFromFile(const std::filesystem::path& filename);

    Query query;
    std::vector<Table> tables_;
    Actions_t actions = {
        {sql::keyword::select, &Sql_interpreter::selectAction},
        {sql::keyword::desc, &Sql_interpreter::describeAction},
        {sql::keyword::describe, &Sql_interpreter::describeAction},
        {sql::keyword::sql_delete, &Sql_interpreter::deleteAction},
        {sql::keyword::create, &Sql_interpreter::createAction},
        {sql::keyword::drop, &Sql_interpreter::dropAction},
        {sql::keyword::show, &Sql_interpreter::showAction},
        {sql::keyword::insert, &Sql_interpreter::insertAction},
        {sql::keyword::alter, &Sql_interpreter::alterAction},
        {sql::keyword::update, &Sql_interpreter::updateAction},
        {sql::keyword::dump, &Sql_interpreter::dumpAction},
        {sql::keyword::source, &Sql_interpreter::sourceAction},
        {sql::keyword::check, &Sql_interpreter::checkAction},
    };
};

// returns error message if occurred
auto check_database(const std::vector<Table>& tables)
    -> std::optional<std::string>;

#endif  // !CUSTOM_SQL_SQL_INTERPRETER_H

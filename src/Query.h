#ifndef CUSTOM_SQL_QUERY_H
#define CUSTOM_SQL_QUERY_H

#include <iostream>
#include <limits>
#include <queue>
#include <sstream>
#include <string>

#include "customSqlExceptions.h"

class Query {
public:
    Query() = default;
    explicit Query(const std::string& input) { set_value(input); }

    void set_value(std::string_view query) {
        constexpr auto SSmax = std::numeric_limits<std::streamsize>::max();
        tokens_ = std::queue<std::string>{};  // clear() queue

        std::istringstream iquery{std::string{query}};
        std::string token;
        while (true) {
            auto ch = static_cast<char>(iquery.get());
            if (not iquery) {
                // remember to add not closed token
                if (not token.empty()) {
                    tokens_.emplace(std::move(token));
                }
                break;
            }

            if (ch == '\'' or ch == '"') {
                if (std::getline(iquery, token, ch) and iquery.eof()) {
                    throw QueryParseErrorException{"Quoted string not closed"};
                }
                tokens_.emplace(std::move(token));
                token.clear();
            } else if (std::isspace(ch) != 0) {
                if (not token.empty()) {
                    tokens_.emplace(std::move(token));
                    token.clear();
                }
                // ignore whitespace characters
            } else if (std::isalnum(ch) != 0 or ch == '-' or ch == '_') {
                token += std::tolower(ch);
                if (token == "--") {
                    iquery.ignore(SSmax, '\n');
                    token.clear();
                }
            } else {  // unknown character
                if (not token.empty()) {
                    tokens_.emplace(std::move(token));
                    token.clear();
                }

                tokens_.emplace(std::string{ch});
            }
        }
    }

    [[nodiscard]] bool areTokensLeft() { return not tokens_.empty(); }

    // returns next token without consuming it
    // if no token is available then return empty string
    auto previewToken() {
        if (tokens_.empty()) {
            return std::string{""};
        }
        return tokens_.front();
    }

    // returns next token then consume it.
    // If no token is available then throws UnexpectedQueryEndException.
    auto getToken() {
        if (tokens_.empty()) {
            throw UnexpectedQueryEndException{};
        }
        auto token = std::move(tokens_.front());
        tokens_.pop();

        return token;
    }

    // consumes next token and checks if it is given value throws
    // QueryParseErrorException if not
    // (kenny) TODO: variadic arguments e.g expectTokens that check if token is
    // in given set of values
    void expect(std::string_view token) {
        if (auto current = getToken(); current != token) {
            throw QueryParseErrorException{"Unexpected token Received '" +
                                           current + "', Expected '" +
                                           std::string{token} + '\''};
        }
    }

    // tokens to ignore
    void ignore(int number = 1) {
        while (not tokens_.empty() and number > 0) {
            tokens_.pop();
            --number;
        }
    }

private:
    std::queue<std::string> tokens_;
};

inline std::string& ltrim(std::string& str,
                          const char* chars_to_ignore = " \t\n") {
    str.erase(0, str.find_first_not_of(chars_to_ignore));
    return str;
}

inline std::string& rtrim(std::string& str,
                          const char* chars_to_ignore = " \t\n") {
    str.erase(str.find_last_not_of(chars_to_ignore) + 1);
    return str;
}

inline std::string& trim(std::string& str,
                         const char* chars_to_ignore = " \t\n") {
    return ltrim(rtrim(str, chars_to_ignore), chars_to_ignore);
}

#endif  // !CUSTOM_SQL_QUERY_H

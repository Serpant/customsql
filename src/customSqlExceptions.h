#ifndef CUSTOM_SQL_EXCEPTIONS_H
#define CUSTOM_SQL_EXCEPTIONS_H
#include <string>

// (kenny): TODO: nest whole program into namespace
class CustomSqlException {
public:
    [[nodiscard]] virtual std::string what() const = 0;

    CustomSqlException() = default;
    virtual ~CustomSqlException() = default;

    CustomSqlException(const CustomSqlException&) = default;
    CustomSqlException& operator=(const CustomSqlException&) = default;

    CustomSqlException(CustomSqlException&&) = default;
    CustomSqlException& operator=(CustomSqlException&&) = default;
};

class NullNotAllowedException : public CustomSqlException {
public:
    explicit NullNotAllowedException(const std::string& columnName)
        : msg_{"Null not allowed in column '" + columnName + '\''} {}

    [[nodiscard]] std::string what() const override { return msg_; }

private:
    std::string msg_;
};

class WrongArgumentTypeException : public CustomSqlException {
public:
    explicit WrongArgumentTypeException(const std::string& columnName,
                                        // (kenny): Column::Type expectedType,
                                        const std::string& expectedType,
                                        const std::string& value)
        : msg_{"Wrong Argument Type in column named '" + columnName +
               "' Expected type '" + expectedType + "' input value '" + value +
               '\''} {}
    [[nodiscard]] std::string what() const override { return msg_; }

private:
    std::string msg_;
};
class WrongArgumentNumberException : public CustomSqlException {
public:
    explicit WrongArgumentNumberException(size_t expected, size_t received)
        : msg_{"Wrong Argument Number, Expected " + std::to_string(expected) +
               ", Received: " + std::to_string(received)} {}
    [[nodiscard]] std::string what() const override { return msg_; }

private:
    std::string msg_;
};

class UnknownNameException : public CustomSqlException {
public:
    explicit UnknownNameException(const std::string& object,
                                  const std::string& name)
        : msg_{"Unknown " + object + " named '" + name + "'"} {}

    [[nodiscard]] std::string what() const override { return msg_; }

private:
    std::string msg_;
};

class FieldExistInRecordException : public CustomSqlException {
public:
    explicit FieldExistInRecordException(const std::string& fieldName)
        : msg_{"Field '" + fieldName + "' exists in record"} {}

    [[nodiscard]] std::string what() const override { return msg_; }

private:
    std::string msg_;
};

class InvalidNameException : public CustomSqlException {
public:
    explicit InvalidNameException(const std::string& name)
        : msg_{"Incorrect name '" + name + '\''} {}

    [[nodiscard]] std::string what() const override { return msg_; }

private:
    std::string msg_;
};

class ColumnAlreadyExistException : public CustomSqlException {
public:
    explicit ColumnAlreadyExistException(const std::string& tableName,
                                         const std::string& columnName)
        : msg_{"In table '" + tableName + "' column with name '" + columnName +
               "' already exists"} {}

    [[nodiscard]] std::string what() const override { return msg_; }

private:
    std::string msg_;
};

class QueryParseErrorException : public CustomSqlException {
public:
    explicit QueryParseErrorException(std::string msg) : msg_{std::move(msg)} {}
    [[nodiscard]] std::string what() const override { return msg_; }

private:
    std::string msg_;
};

class UnexpectedQueryEndException : public CustomSqlException {
public:
    [[nodiscard]] std::string what() const override {
        return "Unexpected end of query";
    }

private:
    std::string msg_;
};

#endif  // !CUSTOM_SQL_EXCEPTIONS_H

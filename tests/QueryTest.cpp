#include "../src/Query.h"
#include "../src/Sql_interpreter.h"
#include <gtest/gtest.h>

TEST(QueryTest, EmptyQuery) {
  Query query{""};

  ASSERT_FALSE(query.areTokensLeft());
  ASSERT_THROW(query.getToken(), UnexpectedQueryEndException);
  ASSERT_EQ(query.previewToken(), "");
}

TEST(QueryTest, oneColumnSelectQuery) {
  Query query{"SELECT id FROM CLIENTS"};

  ASSERT_TRUE(query.areTokensLeft());
  ASSERT_EQ(query.getToken(), sql::keyword::select);

  ASSERT_TRUE(query.areTokensLeft());
  ASSERT_EQ(query.getToken(), "id");

  ASSERT_TRUE(query.areTokensLeft());
  ASSERT_EQ(query.getToken(), sql::keyword::from);

  ASSERT_TRUE(query.areTokensLeft());
  ASSERT_EQ(query.getToken(), "clients");

  ASSERT_FALSE(query.areTokensLeft())
      << "There are tokens left after reading all query";
  ASSERT_EQ(query.previewToken(), "");
  ASSERT_THROW(query.getToken(), UnexpectedQueryEndException);
}

TEST(QueryTest, multipleColumnCommaSeparatedSelectQuery) {
  Query query{"SELECT id, first, name FROM CLIENTS"};

  ASSERT_TRUE(query.areTokensLeft());
  ASSERT_EQ(query.getToken(), sql::keyword::select);

  ASSERT_TRUE(query.areTokensLeft());
  ASSERT_EQ(query.getToken(), "id");
  ASSERT_TRUE(query.areTokensLeft());
  ASSERT_EQ(query.getToken(), ",");

  ASSERT_TRUE(query.areTokensLeft());
  ASSERT_EQ(query.getToken(), "first");
  ASSERT_TRUE(query.areTokensLeft());

  ASSERT_EQ(query.getToken(), ",");
  ASSERT_TRUE(query.areTokensLeft());
  ASSERT_EQ(query.getToken(), "name");

  ASSERT_TRUE(query.areTokensLeft());
  ASSERT_EQ(query.getToken(), sql::keyword::from);

  ASSERT_TRUE(query.areTokensLeft());
  ASSERT_EQ(query.getToken(), "clients");

  ASSERT_FALSE(query.areTokensLeft())
      << "There are tokens left after reading all query";
  ASSERT_THROW(query.getToken(), UnexpectedQueryEndException);
  ASSERT_EQ(query.previewToken(), "");
}

// '-1' number should be one token
TEST(QueryTest, negativeSignHandleGoodFormat) {
  Query query{"SELECT id FROM clients WHERE id = -1"};

  ASSERT_EQ(sql::keyword::select, query.getToken());
  ASSERT_EQ("id", query.getToken());
  ASSERT_EQ(sql::keyword::from, query.getToken());
  ASSERT_EQ("clients", query.getToken());
  ASSERT_EQ(sql::keyword::where, query.getToken());
  ASSERT_EQ("id", query.getToken());
  ASSERT_EQ("=", query.getToken());
  ASSERT_EQ("-1", query.getToken());
}

TEST(QueryTest, negativeSignHandleBadFormat) {
  Query query{"SELECT id FROM clients WHERE id=-1"};

  ASSERT_EQ(sql::keyword::select, query.getToken());
  ASSERT_EQ("id", query.getToken());
  ASSERT_EQ(sql::keyword::from, query.getToken());
  ASSERT_EQ("clients", query.getToken());
  ASSERT_EQ(sql::keyword::where, query.getToken());
  ASSERT_EQ("id", query.getToken());
  ASSERT_EQ("=", query.getToken());
  ASSERT_EQ("-1", query.getToken());

  ASSERT_FALSE(query.areTokensLeft());
  ASSERT_THROW(query.getToken(), UnexpectedQueryEndException);
  ASSERT_EQ(query.previewToken(), "");
}

TEST(QueryTest, HandleStringInsideSingleQuotes) {
  Query query{"INSERT INTO table VALUES 'multiple words text'"};

  ASSERT_EQ("insert", query.getToken());
  ASSERT_EQ("into", query.getToken());
  ASSERT_EQ("table", query.getToken());
  ASSERT_EQ("values", query.getToken());
  ASSERT_EQ("multiple words text", query.getToken());

  ASSERT_FALSE(query.areTokensLeft());
  ASSERT_THROW(query.getToken(), UnexpectedQueryEndException);
  ASSERT_EQ(query.previewToken(), "");
}
TEST(QueryTest, HandleStringInsideDoubleQuotes) {
  Query query{"INSERT INTO table VALUES \"multiple words text\""};

  ASSERT_EQ("insert", query.getToken());
  ASSERT_EQ("into", query.getToken());
  ASSERT_EQ("table", query.getToken());
  ASSERT_EQ("values", query.getToken());
  ASSERT_EQ("multiple words text", query.getToken());

  ASSERT_FALSE(query.areTokensLeft());
  ASSERT_THROW(query.getToken(), UnexpectedQueryEndException);
  ASSERT_EQ(query.previewToken(), "");
}
TEST(QueryTest, QuotedStringQuotedStringNoToLower) {
  Query query{"INSERT INTO table VALUES \"multiple WORDS text\""};

  ASSERT_EQ("insert", query.getToken());
  ASSERT_EQ("into", query.getToken());
  ASSERT_EQ("table", query.getToken());
  ASSERT_EQ("values", query.getToken());
  ASSERT_EQ("multiple WORDS text", query.getToken());

  ASSERT_FALSE(query.areTokensLeft());
  ASSERT_THROW(query.getToken(), UnexpectedQueryEndException);
  ASSERT_EQ(query.previewToken(), "");
}

TEST(QueryTest, HandleNotClosedStringInsideQuotes) {
  ASSERT_THROW(Query query{"INSERT INTO table VALUES'text"},
               QueryParseErrorException)
      << "Exception not thrown when missing ending single quotes (' char)";
  ASSERT_THROW(Query query{"INSERT INTO table VALUES\"text"},
               QueryParseErrorException)
      << "Exception not thrown when missing ending single quotes (\" char)";
}

TEST(QueryTest, HandleParenthesesForSetOfValues) {
  Query query{"INSERT INTO table VALUES (12, 'multiple words text')"};

  ASSERT_EQ(sql::keyword::insert, query.getToken());
  ASSERT_EQ(sql::keyword::into, query.getToken());
  ASSERT_EQ("table", query.getToken());
  ASSERT_EQ(sql::keyword::values, query.getToken());
  ASSERT_EQ("(", query.getToken());
  ASSERT_EQ("12", query.getToken());
  ASSERT_EQ(",", query.getToken());
  ASSERT_EQ("multiple words text", query.getToken());
  ASSERT_EQ(")", query.getToken());

  ASSERT_FALSE(query.areTokensLeft());
  ASSERT_THROW(query.getToken(), UnexpectedQueryEndException);
  ASSERT_EQ(query.previewToken(), "");
}

TEST(QueryTest, HandleSingleLineCommentsAtEndWithSpace) {
  Query query{"SELECT id FROM CLIENTS -- this should be ignored"};

  ASSERT_TRUE(query.areTokensLeft());
  ASSERT_EQ(query.getToken(), sql::keyword::select);

  ASSERT_TRUE(query.areTokensLeft());
  ASSERT_EQ(query.getToken(), "id");

  ASSERT_TRUE(query.areTokensLeft());
  ASSERT_EQ(query.getToken(), sql::keyword::from);

  ASSERT_TRUE(query.areTokensLeft());
  ASSERT_EQ(query.getToken(), "clients");

  ASSERT_FALSE(query.areTokensLeft())
      << "There are tokens left after reading all query";
  ASSERT_THROW(query.getToken(), UnexpectedQueryEndException);
  ASSERT_EQ(query.previewToken(), "");
}

TEST(QueryTest, HandleSingleLineCommentsAtEndWithoutSpace) {
  Query query{"SELECT id FROM CLIENTS --this should be ignored"};

  ASSERT_TRUE(query.areTokensLeft());
  ASSERT_EQ(query.getToken(), sql::keyword::select);

  ASSERT_TRUE(query.areTokensLeft());
  ASSERT_EQ(query.getToken(), "id");

  ASSERT_TRUE(query.areTokensLeft());
  ASSERT_EQ(query.getToken(), sql::keyword::from);

  ASSERT_TRUE(query.areTokensLeft());
  ASSERT_EQ(query.getToken(), "clients");

  ASSERT_FALSE(query.areTokensLeft())
      << "There are tokens left after reading all query";
  ASSERT_THROW(query.getToken(), UnexpectedQueryEndException);
  ASSERT_EQ(query.previewToken(), "");
}

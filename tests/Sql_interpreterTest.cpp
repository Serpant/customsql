#include "../src/Sql_interpreter.h"
#include <gtest/gtest.h>

class Sql_interpreterTest : public ::testing::Test {
protected:
  void SetUp() override { customsql.parseQuery("SOURCE 'clients.custsql'"); }

public:
  Sql_interpreter customsql;
};

TEST_F(Sql_interpreterTest, isProperlyWorking) {}

TEST_F(Sql_interpreterTest, testRenamingColumn) {
  // rename first column
  ASSERT_NE(customsql.tables_begin(), customsql.tables_end())
      << "To do this test we should have at least one column";
  auto &table = *customsql.tables_begin();
  ASSERT_NE(table.getColumnNamed("id"), table.columnEnd())
      << "Column named 'id' does not exist in " << table.name();
  auto &column = *table.getColumnNamed("id");

  ASSERT_EQ(column.name(), "id");
  customsql.parseQuery("ALTER TABLE " + table.name() +
                       " RENAME COLUMN id TO primary_key");
  ASSERT_EQ(column.name(), "primary_key") << "Failed to rename column";
}

TEST_F(Sql_interpreterTest, testRenamingTable) {
  // rename first column
  ASSERT_NE(customsql.tables_begin(), customsql.tables_end())
      << "To do this test we should have at least one column";
  auto &table = *customsql.tables_begin();

  ASSERT_EQ(table.name(), "client");
  customsql.parseQuery("ALTER TABLE " + table.name() + " RENAME TO customers");
  ASSERT_EQ(table.name(), "customers") << "Failed to rename table";
}

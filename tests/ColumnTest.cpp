#include "../src/Column.h"
#include "../src/customSqlExceptions.h"
#include <gtest/gtest.h>

class ColumnTest : public ::testing::Test {
protected:
  void SetUp() override {
    integerColumn.push_back("1");
    integerColumn.push_back("2");
    integerColumn.push_back("3");

    textColumn.push_back("one");
    textColumn.push_back("two");
    textColumn.push_back("three");
  }
  Column integerColumn{"primary_key", Column::Type::Integer};
  Column textColumn{"text_key", Column::Type::Text};
};

TEST_F(ColumnTest, renameColumn) {
  ASSERT_EQ("primary_key", integerColumn.name());
  integerColumn.set_name("id");
  ASSERT_EQ("id", integerColumn.name());

  ASSERT_EQ("text_key", textColumn.name());
  integerColumn.set_name("first_name");
  ASSERT_EQ("first_name", integerColumn.name());
}

TEST_F(ColumnTest, CorrectDescription) {
  auto description = integerColumn.describe();
  ASSERT_EQ("primary_key", description.name);
  ASSERT_EQ(Column::Type::Integer, description.type);
  ASSERT_EQ(true, description.nullable);

  bool not_null = false;
  Column column{"someColumnName", Column::Type::Text, 0, not_null};
  auto column_description = column.describe();
  ASSERT_EQ("someColumnName", column_description.name);
  ASSERT_EQ(Column::Type::Text, column_description.type);
  ASSERT_EQ(false, column_description.nullable);
}

TEST_F(ColumnTest, TwoArgumentConstructor) {
  Column column{"someColumnName", Column::Type::Integer};

  ASSERT_EQ(Column::Type::Integer, column.type()) << "Column type wrong";
  ASSERT_EQ("someColumnName", column.name()) << "Column name wrong";
  ASSERT_EQ(0, column.size()) << "Size different than 0";
  ASSERT_EQ(column.begin(), column.end())
      << "begin() and end() are not the same";
}

TEST_F(ColumnTest, ThreeArgumentConstructor) {
  constexpr int given_size = 80;
  Column column{"someColumnName", Column::Type::Integer, given_size};

  ASSERT_EQ(Column::Type::Integer, column.type()) << "Column type wrong";
  ASSERT_EQ("someColumnName", column.name()) << "Column name wrong";
  ASSERT_EQ(given_size, column.size()) << "Size different than " << given_size;
  ASSERT_NE(column.begin(), column.end()) << "begin() and end() the same";
}

TEST_F(ColumnTest, fourArgumentConstructorNullable) {
  constexpr int given_size = 0;
  bool nullable = true;
  Column column{"someColumnName", Column::Type::Integer, given_size, nullable};

  ASSERT_EQ(Column::Type::Integer, column.type()) << "Column type wrong";
  ASSERT_EQ("someColumnName", column.name()) << "Column name wrong";
  ASSERT_EQ(given_size, column.size()) << "Size different than " << given_size;
  ASSERT_EQ(column.begin(), column.end()) << "begin() and end() not the same";
  ASSERT_TRUE(column.nullable());
}

TEST_F(ColumnTest, fourArgumentConstructorNotNull) {
  constexpr int given_size = 0;
  bool nullable = false;
  Column column{"someColumnName", Column::Type::Integer, given_size, nullable};

  ASSERT_EQ(Column::Type::Integer, column.type()) << "Column type wrong";
  ASSERT_EQ("someColumnName", column.name()) << "Column name wrong";
  ASSERT_EQ(given_size, column.size()) << "Size different than " << given_size;
  ASSERT_EQ(column.begin(), column.end()) << "begin() and end() not the same";
  ASSERT_FALSE(column.nullable());
}

TEST_F(ColumnTest, AddingCorrectValueType) {
  Column freshIntegerColumn{"someColumnName", Column::Type::Integer};
  ASSERT_EQ(0, freshIntegerColumn.size());

  freshIntegerColumn.push_back("1");
  ASSERT_EQ(1, freshIntegerColumn.size());
  ASSERT_EQ("1", freshIntegerColumn[0].value());

  freshIntegerColumn.push_back("2");
  ASSERT_EQ(2, freshIntegerColumn.size());
  ASSERT_EQ("2", freshIntegerColumn[1].value());

  freshIntegerColumn.push_back("3");
  ASSERT_EQ(3, freshIntegerColumn.size());
  ASSERT_EQ("3", freshIntegerColumn[2].value());

  ASSERT_EQ(3, integerColumn.size());
  integerColumn.push_back("4");
  ASSERT_EQ(4, integerColumn.size());
  ASSERT_EQ("4", integerColumn[3].value());

  ASSERT_EQ(3, textColumn.size());
  textColumn.push_back("someText");
  ASSERT_EQ(4, textColumn.size());
  ASSERT_EQ("someText", textColumn[3].value());
}

TEST_F(ColumnTest, AddingWrongValueType) {
  Column freshIntegerColumn{"someColumnName", Column::Type::Integer};
  ASSERT_EQ(0, freshIntegerColumn.size());
  ASSERT_EQ(Column::Type::Integer, integerColumn.type());
  ASSERT_THROW(freshIntegerColumn.push_back("someText"),
               WrongArgumentTypeException)
      << "Exception not throw when adding wrong type value";
  ASSERT_EQ(Column::Type::Integer, integerColumn.type());
  ASSERT_EQ(0, freshIntegerColumn.size());

  ASSERT_EQ(3, integerColumn.size());
  ASSERT_EQ(Column::Type::Integer, integerColumn.type());
  ASSERT_THROW(integerColumn.push_back("someText"), WrongArgumentTypeException);
  ASSERT_EQ(Column::Type::Integer, integerColumn.type());
  ASSERT_EQ(3, integerColumn.size());
}

TEST_F(ColumnTest, AddingNullValue) {
  Column freshIntegerColumn{"someColumnName", Column::Type::Integer};
  ASSERT_EQ(Column::Type::Integer, freshIntegerColumn.type());
  ASSERT_EQ(0, freshIntegerColumn.size());
  freshIntegerColumn.push_back(std::nullopt);
  ASSERT_EQ(Column::Type::Integer, freshIntegerColumn.type());
  ASSERT_EQ(1, freshIntegerColumn.size());

  ASSERT_EQ(Column::Type::Text, textColumn.type());
  ASSERT_EQ(3, textColumn.size());
  textColumn.push_back(std::nullopt);
  ASSERT_EQ(Column::Type::Text, textColumn.type());
  ASSERT_EQ(4, textColumn.size());
}

TEST_F(ColumnTest, AddingNullValueToNotNullColumn) {
  bool nullable = false;
  Column notNullColumn{"someColumnName", Column::Type::Integer, 0, nullable};
  ASSERT_EQ(Column::Type::Integer, notNullColumn.type());
  ASSERT_EQ(0, notNullColumn.size());

  ASSERT_THROW(notNullColumn.push_back(std::nullopt), NullNotAllowedException);

  ASSERT_EQ(Column::Type::Integer, notNullColumn.type());
  ASSERT_EQ(0, notNullColumn.size());
}

TEST_F(ColumnTest, RemovingValues) {
  Column freshColumn{"someColumnName", Column::Type::Integer};
  ASSERT_EQ(0, freshColumn.size());
  ASSERT_EQ(Column::Type::Integer, freshColumn.type());
  ASSERT_EQ(freshColumn.begin(), freshColumn.end())
      << "Iterator returned by begin() and end() are not indicating that "
         "column is empty";
  freshColumn.push_back("1");
  ASSERT_EQ(1, freshColumn.size());

  auto pos = freshColumn.begin();
  ASSERT_NE(pos, freshColumn.end());
  freshColumn.erase(pos);
  ASSERT_EQ(0, freshColumn.size());

  ASSERT_EQ(3, textColumn.size());
  auto second = ++textColumn.begin();
  textColumn.erase(second);
  ASSERT_EQ(2, textColumn.size());

  int size = textColumn.size();
  while (textColumn.size() != 0) {
    ASSERT_EQ(size, textColumn.size());
    textColumn.erase(textColumn.begin());
    --size;
  }
}

TEST(ColumnTypeAsStringTest, isCorrectReturnValue) {
  ASSERT_EQ("Text", ColumnTypeAsString(Column::Type::Text));
  ASSERT_EQ("Integer", ColumnTypeAsString(Column::Type::Integer));
}

TEST(isCorrectIntegerTest, EmptyString) { ASSERT_FALSE(isCorrectInteger("")); }

TEST(isCorrectIntegerTest, CorrectInteger) {
  ASSERT_TRUE(isCorrectInteger("1"));
  ASSERT_TRUE(isCorrectInteger("-1")) << "Negative sign not handled correctly";
  ASSERT_TRUE(isCorrectInteger("123"));
  ASSERT_TRUE(isCorrectInteger("-123"))
      << "Negative sign not handled correctly";
}

TEST(isCorrectIntegerTest, IncorrectInteger) {
  ASSERT_FALSE(isCorrectInteger("-")) << "Negative sign not handled correctly";

  ASSERT_FALSE(isCorrectInteger("21fds23"));
  ASSERT_FALSE(isCorrectInteger("-12fdslkj23"));

  ASSERT_FALSE(isCorrectInteger("d"))
      << "single character not handled correctly";
  ASSERT_FALSE(isCorrectInteger("-d"))
      << "single character with Negative sign not handled correctly";
  ASSERT_FALSE(isCorrectInteger("-dfds"))
      << "Negative sign string not handled correctly";
}

TEST(isCorrectType, isCorrectReturnValue) {
  ASSERT_TRUE(isCorrectType(Column::Type::Text, "sometext"));
  ASSERT_TRUE(isCorrectType(Column::Type::Text, "some text"))
      << "Text with space";
  ASSERT_TRUE(isCorrectType(Column::Type::Text, "")) << "empty value";

  ASSERT_TRUE(isCorrectType(Column::Type::Integer, "123"));
  ASSERT_TRUE(isCorrectType(Column::Type::Integer, "-123"))
      << "number with negative sign";

  ASSERT_FALSE(isCorrectType(Column::Type::Integer, ""))
      << "empty value not handled correctly";
  ASSERT_FALSE(isCorrectType(Column::Type::Integer, "-ddfs"));
  ASSERT_FALSE(isCorrectType(Column::Type::Integer, "ddfs"))
      << "number with negative sign";
}

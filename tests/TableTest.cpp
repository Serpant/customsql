#include "../src/Table.h"
#include <gtest/gtest.h>

class TableTest : public ::testing::Test {
protected:
  void SetUp() override {
    preparedTable.add_column("id", Column::Type::Integer);
    preparedTable.add_column("firstname", Column::Type::Text);
    preparedTable.add_column("lastname", Column::Type::Text);

    std::vector<std::pair<std::string, std::string>> first_and_last_names = {
        {"maciek", "krzywda"},     {"tomasz", "zawadiaka"},
        {"janusz", "andrzejczuk"}, {"janusz", "bak"},
        {"krystyna", "wieczorek"}, {"klaudia", "miras"},
    };
    int i = 0;
    for (const auto &[firstName, lastName] : first_and_last_names) {
      auto record = preparedTable.get_form();
      record.get_field("id") = std::to_string(i);
      record.get_field("firstname") = firstName;
      record.get_field("lastname") = lastName;

      preparedTable.add_record(std::move(record));
      ++i;
    }
  }

  Table preparedTable{"fixtureTable"};
};

TEST_F(TableTest, testAddingColumn) {
  Table localTable{"localTable"};
  ASSERT_EQ(0, localTable.columnNumber());
  ASSERT_EQ(0, localTable.recordsNumber());

  localTable.add_column("id", Column::Type::Integer);
  ASSERT_EQ(1, localTable.columnNumber());
  ASSERT_EQ(0, localTable.recordsNumber());
  localTable.add_column("name", Column::Type::Text);
  ASSERT_EQ(2, localTable.columnNumber());
  ASSERT_EQ(0, localTable.recordsNumber());
  localTable.add_column("surname", Column::Type::Text);
  ASSERT_EQ(3, localTable.columnNumber());
  ASSERT_EQ(0, localTable.recordsNumber());

  ASSERT_EQ(3, preparedTable.columnNumber());
  ASSERT_EQ(6, preparedTable.recordsNumber());
  preparedTable.add_column("dmg", Column::Type::Integer);
  ASSERT_EQ(4, preparedTable.columnNumber());
  ASSERT_EQ(6, preparedTable.recordsNumber());
}

TEST_F(TableTest, testAddingColumnWithNameThatExist) {
  Table localTable{"localTable"};
  localTable.add_column("someColumnName", Column::Type::Integer);
  ASSERT_EQ(1, localTable.columnNumber());

  ASSERT_THROW(localTable.add_column("someColumnName", Column::Type::Integer),
               ColumnAlreadyExistException)
      << "Exception not thrown when adding column with the same name and type";
  ASSERT_EQ(1, localTable.columnNumber())
      << "Column number has changed after add_column() failure";

  ASSERT_THROW(localTable.add_column("someColumnName", Column::Type::Text),
               ColumnAlreadyExistException)
      << "Exception not thrown when adding column with the same name and "
         "different type";
  ASSERT_EQ(1, localTable.columnNumber())
      << "Column number has changed after add_column() failure";
}

TEST_F(TableTest, testAddingCorrectRecords) {
  ASSERT_EQ(6, preparedTable.recordsNumber());
  ASSERT_EQ(3, preparedTable.columnNumber());

  for (int i = 0; i < 2; ++i) {
    Record record = preparedTable.get_form();
    record.get_field("id") = std::to_string(i);
    record.get_field("firstname") = std::to_string('a' + i);
    record.get_field("lastname") = "Paarturnax";
    preparedTable.add_record(std::move(record));
  }
  ASSERT_EQ(8, preparedTable.recordsNumber());
}

TEST_F(TableTest, testAddingRecordsWithIncorrectTypes) {
  auto before_inserting = preparedTable.recordsNumber();
  auto recordWrongIntegerValue = preparedTable.get_form();
  recordWrongIntegerValue.get_field("id") = "d";
  recordWrongIntegerValue.get_field("firstname") = "Andrzej";
  recordWrongIntegerValue.get_field("lastname") = "Andrzejczuk";

  ASSERT_EQ(before_inserting, preparedTable.recordsNumber());
  ASSERT_THROW(preparedTable.add_record(recordWrongIntegerValue),
               WrongArgumentTypeException);
  ASSERT_EQ(before_inserting, preparedTable.recordsNumber());
}
TEST_F(TableTest, testRemovingByIndex) {
  auto before_removing = preparedTable.recordsNumber();

  ASSERT_EQ(before_removing, preparedTable.recordsNumber());
  preparedTable.remove_record(preparedTable.recordsNumber() + 1);
  ASSERT_EQ(before_removing, preparedTable.recordsNumber())
      << "Removed record when index was out of range";

  ASSERT_EQ(before_removing, preparedTable.recordsNumber());
  preparedTable.remove_record(0);
  ASSERT_EQ(before_removing - 1, preparedTable.recordsNumber());

  Table freshTable{"freshTableName"};
  ASSERT_EQ(0, freshTable.recordsNumber());
  freshTable.remove_record(0);
  ASSERT_EQ(0, freshTable.recordsNumber())
      << "Empty table but size was changed when removing by index";
}

TEST_F(TableTest, testRemovingByRecord_iterator) {
  // test removing only one
  auto before = preparedTable.recordsNumber();
  ASSERT_EQ(before, preparedTable.recordsNumber());
  preparedTable.remove_record(preparedTable.begin());
  ASSERT_EQ(before - 1, preparedTable.recordsNumber());

  --before;
  // pop_front
  while (preparedTable.begin() != preparedTable.end()) {
    ASSERT_EQ(before, preparedTable.recordsNumber());
    preparedTable.remove_record(preparedTable.begin());
    ASSERT_EQ(before - 1, preparedTable.recordsNumber());
    --before;
  }

  ASSERT_EQ(0, before);
}

TEST_F(TableTest, testTableFinding) {
  ASSERT_THROW(auto p = preparedTable.find(Field{"UnknownColumnName", "value"}),
               UnknownNameException);
  auto oneResult = preparedTable.find(Field{"id", "0"});
  ASSERT_EQ(1, oneResult.size());

  auto twoResults = preparedTable.find(Field{"firstname", "janusz"});
  ASSERT_EQ(2, twoResults.size())
      << "preparedTable contains two record with 'firstname' = 'janusz' that "
         "were not found";
  auto emptyResult = preparedTable.find(Field{"id", "1323"});
  ASSERT_EQ(0, emptyResult.size());
  ASSERT_TRUE(emptyResult.empty());
}

TEST(isCorrectColumnNameTest, resultTest) {
  ASSERT_FALSE(isValidName("")) << "cannot be empty";
  ASSERT_FALSE(isValidName("Primary Key")) << "cannot contain spaces";

  ASSERT_TRUE(isValidName("primary_key"));
  ASSERT_TRUE(isValidName("p"));
}

add_executable(ColumnTest ColumnTest.cpp ../src/Column.cpp)
add_executable(TableTest TableTest.cpp ../src/Table.cpp ../src/Column.cpp)
add_executable(
  Sql_interpreterTest Sql_interpreterTest.cpp ../src/Sql_interpreter.cpp
                      ../src/Table.cpp ../src/Column.cpp)
add_executable(QueryTest QueryTest.cpp)

target_link_libraries(
  ColumnTest # target name
  gtest # testing framework
  gtest_main # gtest should add own main function
)
target_link_libraries(
  TableTest # target name
  gtest # testing framework
  gtest_main # gtest should add own main function
)
target_link_libraries(
  Sql_interpreterTest # target name
  gtest # testing framework
  gtest_main # gtest should add own main function
)
target_link_libraries(
  QueryTest # target name
  gtest # testing framework
  gtest_main # gtest should add own main function
)

add_test(ColumnTest ColumnTest)
add_test(TableTest TableTest)
add_test(Sql_interpreterTest Sql_interpreterTest)
add_test(QueryTest QueryTest)
